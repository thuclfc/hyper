function Calendar(month, year) {
    var now = new Date();

    // labels for week days and months
    var days_labels = ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
        months_labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'Aug', 'September', 'October', 'November', 'December'];

    // test if input date is correct, instead use current month
    this.month = (isNaN(month) || month == null) ? now.getMonth() + 1 : month;
    this.year = (isNaN(year) || year == null) ? now.getFullYear() : year;

    var logical_month = this.month - 1;

    // get first day of month and first week day
    var first_day = new Date(this.year, logical_month, 1),
        first_day_weekday = first_day.getDay() == 0 ? 7 : first_day.getDay();

    // find number of days in month
    var month_length = new Date(this.year, this.month, 0).getDate(),
        previous_month_length = new Date(this.year, logical_month, 0).getDate();

    // calendar
    var html = '';

    // calendar content


    // define default day variables
    var day  = 1, // current month days
        prev = 1, // previous month days
        next = 1; // next month days


    html += '<div class="week">';
    // weeks loop (rows)
    for (var i = 0; i < 9; i++) {
        html +='<div class="week_month">'+ months_labels[logical_month] +'</div>';
        html += '<div class="week_label flex">';
        for (var k = 0; k <= 6; k++) {
            html += '<div>';
            html += days_labels[k];
            html += '</div>';
        }
        html += '</div>';
        // weekdays loop (cells)
        html += '<div class="week_day flex">';
        for (var j = 1; j <= 7; j++) {
            if (day <= month_length && (i > 0 || j >= first_day_weekday)) {
                // current month
                html += '<div class="day">';
                html += day;
                html += '</div>';
                day++;
            } else {
                if (day <= month_length) {
                    // previous month
                    html += '<div class="day other-month">';
                    html += previous_month_length - first_day_weekday + prev + 1;
                    html += '</div>';
                    prev++;
                } else {
                    // next month
                    html += '<div class="day other-month">';
                    html += next;
                    html += '</div>';
                    next++;
                }
            }
        }
        html +='</div>'
        // stop making rows if it's the end of month
        if (day > month_length) {
            html += '</div>';
            break;
        } else {
            html += '</div><div class="week">';
        }
    }


    return html;
}

document.getElementById('calendar').innerHTML = Calendar();
